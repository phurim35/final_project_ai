﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    public Camera myCamera;
    public Transform myLight;
    public Transform myLight2;
    public Transform myLight3;
    public Transform moreRay1;
    public Transform moreRay2;

    public Light activeLight;
    public LayerMask Enemylayer;

    public EnemyControllMo enemy;
    public bool lightOn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            activeLight.gameObject.SetActive(true);
            lightOn = true;
        }
        
        if (Input.GetMouseButtonUp(0))
        {
            activeLight.gameObject.SetActive(false);
            lightOn = false;
        }

        Vector3 forward =myLight.transform.TransformDirection(Vector3.forward) * 10;

        Vector3 left = myLight2.transform.TransformDirection(Vector3.forward) * 10;
        Vector3 right = myLight3.transform.TransformDirection(Vector3.forward) * 10;

        Vector3 moreRayLeft = moreRay1.transform.TransformDirection(Vector3.forward) * 10;
        Vector3 moreRayRight = moreRay2.transform.TransformDirection(Vector3.forward) * 10;







        RaycastHit hit;
        //enemy.isFace = false;
        if (Physics.Raycast(myLight.transform.position, forward, out hit, 10, Enemylayer))
            {
            enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
            enemy.isFace = true;

                if (enemy.isFace == true)
                {
                    Debug.Log("face");
                    //enemy.FaceTarget();
                }


            }

            if (Physics.Raycast(moreRay1.transform.position, moreRayLeft, out hit, 10, Enemylayer))
            {
            enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
            enemy.isFace = true;

                if (enemy.isFace == true)
                {
                    Debug.Log("face");
                    //enemy.FaceTarget();
                }


            }

            if (Physics.Raycast(moreRay2.transform.position, moreRayRight, out hit, 10, Enemylayer))
            {
            enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
            enemy.isFace = true;

                if (enemy.isFace == true)
                {
                    Debug.Log("face");
                    //enemy.FaceTarget();
                }


            }

            if (Physics.Raycast(myLight2.transform.position, left, out hit, 10, Enemylayer))
            {
            enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
            enemy.isFace = true;
                if (enemy.isFace == true)
                {
                    //enemy.FaceTarget();
                }
            }

            if (Physics.Raycast(myLight3.transform.position, right, out hit, 10, Enemylayer))
            {
            enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
            enemy.isFace = true;
                if (enemy.isFace == true)
                {
                    //enemy.FaceTarget();
                }
            }
        
        

        //light
            if (lightOn == true)
        {
            enemy.isFace = false;
            enemy.changDirection = 1;
            if (Physics.Raycast(myLight.transform.position, forward, out hit, 10, Enemylayer))
            {
                enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
                enemy.changDirection = -1;
                Debug.Log("Did Hit");

            }

            if (Physics.Raycast(myLight2.transform.position, left, out hit, 10, Enemylayer))
            {
                enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
                enemy.changDirection = -1;
                Debug.Log("Did Hit");

            }

            if (Physics.Raycast(myLight3.transform.position, right, out hit, 10, Enemylayer))
            {
                enemy = hit.collider.gameObject.GetComponent<EnemyControllMo>();
                enemy.changDirection = -1;
                Debug.Log("Did Hit");

            }
        }     
        
        

    }
}

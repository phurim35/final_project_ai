﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

public class EnemyControllMo : MonoBehaviour
{
    public ThirdPersonModify character;

    public float lookRadius = 15f;
    private Vector3 m_Move;


    public int changDirection =-1;

    Vector3 direction;
    public Transform target;
    NavMeshAgent agent;
    public bool hitWall=false;
    bool onMove = true;

    Vector3 interceptionDistance;

    public bool isFace;

    // Start is called before the first frame update
    void Start()
    {
        target = PlayerManager.instance.player.transform;

        agent = GetComponent<NavMeshAgent>();
        character = GetComponent<ThirdPersonModify>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Interception();
        FlashLight light;
        light = target.GetComponent<FlashLight>();

        if (onMove == true)
        {
            agent.speed = 0;
            agent.SetDestination(new Vector3(0, 0, 0));


            float distance = Vector3.Distance(target.position, transform.position);

            if (distance > lookRadius)
            {
                changDirection = 0;
                Move();
            }
            if (distance <= lookRadius)
            {
                if (isFace == false)
                {
                    m_Move = interceptionDistance;
                }
                

                //Debug.Log("" + interceptionDistance);
                character.m_MoveSpeedMultiplier = 0.8f;

                if (isFace == true)
                {
                    m_Move = target.position - transform.position;
                }
                



                if (light.lightOn == false)
                {

                    
                    changDirection = 1;
                    
                    
                }
                

                Move();


                if (distance <= 5)
                {
                    // Atack the target
                    //FaceTarget();
                }
                

            }
        }
        if (hitWall == true)
        {
            character.m_MoveSpeedMultiplier = 0;
            agent.SetDestination(target.position);
            agent.speed = 3.5f;



        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {

            hitWall = true;
            onMove = false;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            onMove = true;
            hitWall = false;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        
            
    }
    public void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;

        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

        m_Move = target.position - transform.position;
    }



    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
    private void Move()
    {
        
        character.Move(m_Move * changDirection, false, false);
    }
    private void Interception()
    {
        

        Vector3  Sr, St;
        float Vr;
        float tc;


        CharacterMovementModify characterMovementModify = target.gameObject.GetComponent<CharacterMovementModify>();
        ThirdPersonModify thirdPersonModify = this.gameObject.GetComponent<ThirdPersonModify>();

        Vr = characterMovementModify.speed - thirdPersonModify.m_MoveSpeedMultiplier;
        Sr = target.gameObject.transform.position - this.gameObject.transform.position;

        tc= Sr.magnitude / Vr;

        St = target.gameObject.transform.position + (characterMovementModify.targetDirection * tc);
        interceptionDistance = St - this.gameObject.transform.position;
    }
}

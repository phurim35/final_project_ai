﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{


    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class EnemyController : MonoBehaviour
    {
        public ThirdPersonCharacter character;

        public float lookRadius = 10f;
        private Vector3 m_Move;

        public int changDirection=1;

        Vector3 direction;
        public Transform target;
        NavMeshAgent agent;

        // Start is called before the first frame update
        void Start()
        {
            //target = PlayerManager.instance.player.transform;

            agent = GetComponent<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {

            
            float distance = Vector3.Distance(target.position, transform.position);

            if (distance <= lookRadius)
            {
                //agent.SetDestination(target.position);
                m_Move = target.position - transform.position;


                character.Move(m_Move* changDirection, false,false);
    

            if (distance <= 1)
                {
                    // Atack the target
                    FaceTarget();
                }

            }
        }

        void FaceTarget()
        {
            Vector3 direction = (target.position - transform.position).normalized;

            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);
        }



        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, lookRadius);
        }

    }
}
